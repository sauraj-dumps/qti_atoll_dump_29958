## qssi-user 11 RKQ1.201112.002 eng.root.20220219.003233 release-keys
- Manufacturer: qualcomm
- Platform: atoll
- Codename: atoll
- Brand: qti
- Flavor: qssi-user
- Release Version: 11
- Id: RKQ1.201112.002
- Incremental: eng.root.20220219.003233
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: qti/atoll/atoll:11/RKQ1.201112.002/root02190104:user/release-keys
- OTA version: 
- Branch: qssi-user-11-RKQ1.201112.002-eng.root.20220219.003233-release-keys
- Repo: qti_atoll_dump_29958


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
